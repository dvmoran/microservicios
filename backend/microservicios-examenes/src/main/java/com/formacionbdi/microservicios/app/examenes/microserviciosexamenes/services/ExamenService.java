package com.formacionbdi.microservicios.app.examenes.microserviciosexamenes.services;

import java.util.List;

import com.formacionbdi.microservicios.commons.examenes.commonsexamenes.models.entity.Asignatura;
import com.formacionbdi.microservicios.commons.examenes.commonsexamenes.models.entity.Examen;
import com.formacionbdi.microservicios.commons.microservicioscommons.services.CommonService;

public interface ExamenService extends CommonService<Examen> {
    public List<Examen> findByNombre(String term);

    public Iterable<Asignatura> findAllAsignaturas();

    public Iterable<Long> findExamenesIdsConRespuestaByPreguntaIds(Iterable<Long> preguntaIds); 
}
