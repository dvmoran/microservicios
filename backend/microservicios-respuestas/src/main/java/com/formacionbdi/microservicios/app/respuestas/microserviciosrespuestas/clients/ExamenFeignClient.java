package com.formacionbdi.microservicios.app.respuestas.microserviciosrespuestas.clients;

import java.util.List;

import com.formacionbdi.microservicios.commons.examenes.commonsexamenes.models.entity.Examen;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "microservicios-examenes")
public interface ExamenFeignClient {
    
    @GetMapping("/{id}")
    public Examen obtenerExamenPorId(@PathVariable Long id);

    @GetMapping("/respondidos-por-preguntas")
    public List<Long> obtenerExamenesporPreguntasIdRespondidas(@RequestParam List<Long> preguntaIds);
}
