package com.formacionbdi.microservicios.app.respuestas.microserviciosrespuestas.services;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import com.formacionbdi.microservicios.app.respuestas.microserviciosrespuestas.clients.ExamenFeignClient;
import com.formacionbdi.microservicios.app.respuestas.microserviciosrespuestas.models.entity.Respuesta;
import com.formacionbdi.microservicios.app.respuestas.microserviciosrespuestas.models.repository.RespuestaRepository;
import com.formacionbdi.microservicios.commons.examenes.commonsexamenes.models.entity.Examen;
import com.formacionbdi.microservicios.commons.examenes.commonsexamenes.models.entity.Pregunta;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Service
public class RespuestaServiceImpl implements RespuestaService {

    @Autowired
    private RespuestaRepository repository;

    @Autowired
    private ExamenFeignClient examenClient;

    @Override

    public Iterable<Respuesta> saveAll(Iterable<Respuesta> respuestas) {
        return repository.saveAll(respuestas);
    }

    @Override
    public Iterable<Respuesta> findRespuestasByAlumnoByExamen(Long alumnoId, Long examenId) {
        Examen examen = examenClient.obtenerExamenPorId(examenId);
        List<Pregunta> preguntas = examen.getPreguntas();
        List<Long> preguntaIds = preguntas.stream().map(p-> p.getId()).collect(Collectors.toList());
        List<Respuesta> respuestas = (List<Respuesta>) repository.findRespuestaByAlumnoByPreguntaIds(alumnoId, preguntaIds);
        respuestas = respuestas.stream().map(r->{
            preguntas.forEach(p->{
                if(p.getId() == r.getPreguntaId()){
                    r.setPregunta(p);
                }
            });
            return r;
        }).collect(Collectors.toList());
        
        return respuestas;
    }

    @Override
    public Iterable<Long> findExamenesIdsConRespuestaByAlumno(Long alumnoId) {
        List<Respuesta>  respuestasAlumno = (List<Respuesta>) repository.findByAlumnoId(alumnoId);
        List<Long> examenIds = Collections.emptyList();
        if (respuestasAlumno.size() > 0){
            List<Long> preguntaIds = respuestasAlumno.stream().map(r -> r.getPreguntaId()).collect(Collectors.toList());
            examenIds = examenClient.obtenerExamenesporPreguntasIdRespondidas(preguntaIds);
        }
        return examenIds;
    }

    @Override
    public Iterable<Respuesta> findByAlumnoId(Long alumnoId) {
        return repository.findByAlumnoId(alumnoId);
    }
    
}
