package com.formacionbdi.microservicios.app.cursos.microservicioscursos.services;

import java.util.List;

import com.formacionbdi.microservicios.app.cursos.microservicioscursos.clients.AlumnoFeignClient;
import com.formacionbdi.microservicios.app.cursos.microservicioscursos.clients.RespuestaFeingClient;
import com.formacionbdi.microservicios.app.cursos.microservicioscursos.models.entity.Curso;
import com.formacionbdi.microservicios.app.cursos.microservicioscursos.models.repository.CursoRepository;
import com.formacionbdi.microservicios.common.alumnos.models.entity.Alumno;
import com.formacionbdi.microservicios.commons.microservicioscommons.services.CommonServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CursoServiceImpl extends CommonServiceImpl<Curso, CursoRepository> implements CursosService {

    @Autowired
    private RespuestaFeingClient client;

    @Autowired
    private AlumnoFeignClient clientAlumno;

    @Override
    @Transactional(readOnly = true)
    public Curso findCursoById(Long id) {
        return repository.findCursoById(id);
    }

    @Override
    public Iterable<Long> obtenerExamenesIdsConRespuestasAlumno(Long alumnoId) {
        return client.obtenerExamenesIdsConRespuestasAlumno(alumnoId);
    }

    @Override
    public Iterable<Alumno> obtenerAlumnoPorCurso(List<Long> ids) {
        return clientAlumno.obtenerAlumnoPorCurso(ids);
    }
    
    @Transactional
    public void eliminarCursoAlumnoPorId(Long id){
        repository.eliminarCursoAlumnoPorId(id);
    }
}
