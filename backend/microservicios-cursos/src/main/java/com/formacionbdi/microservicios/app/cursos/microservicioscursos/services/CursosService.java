package com.formacionbdi.microservicios.app.cursos.microservicioscursos.services;

import java.util.List;

import com.formacionbdi.microservicios.app.cursos.microservicioscursos.models.entity.Curso;
import com.formacionbdi.microservicios.common.alumnos.models.entity.Alumno;
import com.formacionbdi.microservicios.commons.microservicioscommons.services.CommonService;

public interface CursosService extends CommonService<Curso> {
    public Curso findCursoById(Long id);

    public Iterable<Long> obtenerExamenesIdsConRespuestasAlumno(Long alumnoId);

    public Iterable<Alumno> obtenerAlumnoPorCurso(List<Long> ids);

    public void eliminarCursoAlumnoPorId(Long id);
}
