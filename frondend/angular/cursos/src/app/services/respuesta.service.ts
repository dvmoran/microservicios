import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BASE_ENDPOINT } from '../config/app';
import { Alumno } from '../models/alumno';
import { Examen } from '../models/examen';
import { Respuestas } from '../models/respuestas';

@Injectable({
  providedIn: 'root'
})
export class RespuestaService {

  private cabeceras: HttpHeaders = new HttpHeaders({'Content-Type': 'application/json'});

  private baseEndpoint = BASE_ENDPOINT + '/respuestas';

  constructor(private http: HttpClient) { }


  crear(respuestas: Respuestas[]): Observable<Respuestas[]>{
    return this.http.post<Respuestas[]>(this.baseEndpoint, respuestas, {headers: this.cabeceras});
  }


  obtenerRespuestasPorAlumnoPorExamen(alumno: Alumno, examen: Examen): Observable<Respuestas[]>{
    return this.http.get<Respuestas[]>(`${this.baseEndpoint}/alumno/${alumno.id}/examen/${examen.id}`);
  }
}
