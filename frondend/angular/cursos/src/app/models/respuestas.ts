import { Alumno } from "./alumno";
import { Pregunta } from "./pregunta";

export class Respuestas {
    id: string;
    texto: string;
    alumno: Alumno;
    pregunta: Pregunta;
}
