import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Alumno } from '../models/alumno';
import { Curso } from '../models/curso';
import { Examen } from '../models/examen';
import { Pregunta } from '../models/pregunta';
import { Respuestas } from '../models/respuestas';

@Component({
  selector: 'app-responder-examen-modal',
  templateUrl: './responder-examen-modal.component.html',
  styleUrls: ['./responder-examen-modal.component.css']
})
export class ResponderExamenModalComponent implements OnInit {

  curso: Curso;
  alumno: Alumno;
  examen: Examen;

  respuestas: Map<number, Respuestas> = new Map<number, Respuestas>();

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
  public modelRef: MatDialogRef<ResponderExamenModalComponent>)  { }

  ngOnInit(): void {
    this.curso = this.data.curso as Curso;
    this.alumno = this.data.alumno as Alumno;
    this.examen = this.data.examen as Examen;
  }

  cancelar(): void{
    this.modelRef.close();
  }

  responder(pregunta: Pregunta, event){
    const texto = event.target.value as string;
    const respuesta = new Respuestas();
    respuesta.alumno = this.alumno;
    respuesta.pregunta = pregunta;

    const examen = new Examen();
    examen.id = this.examen.id;
    examen.nombre = this.examen.nombre;

    respuesta.pregunta.examen = examen;
    respuesta.texto = texto;

    this.respuestas.set(pregunta.id, respuesta);
    console.log(this.respuestas)

  }
}
